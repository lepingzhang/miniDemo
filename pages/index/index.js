Page({
  data: {
   
  },
  clickIntoFloatButton:function(){
    wx.navigateTo({
      url: '/pages/floatButton/floatButton',
    })
  },
  clickIntoSvg: function () {
    wx.navigateTo({
      url: '/pages/svgIcon/svgIcon',
    })
  },
  routerClickInto:function(e){
    let router=e.currentTarget.dataset.router
    wx.navigateTo({
      url: `/pages/${router}/${router}`,
    })
  }
})