// let videoContext=""    
Page({
  onReady: function(res) {
    this.videoContext = wx.createVideoContext('myVideo_0')
    this.videoContext.seek(0)
    // this.videoContext.play()
  },
  inputValue: '',
  data: {
    src: '',
    selectedIndex: "",
    subtitles: [],
    toView: ""
  },
  onLoad: function() {
    this.getSrtList()
  },
  onShow: function() {
    setTimeout(() => {
      this.clickInto()
    }, 200)

  },
  videoChange: function(e) {
    this.videoContext.pause()
    console.log(e)
    console.log(this.videoContext)
    let index = e.detail.current
    this.videoContext = wx.createVideoContext('myVideo_' + index)
    this.videoContext.seek(0)
    this.videoContext.play()
  },
  videoUpdateTime: function(e) {
    // console.log(e)
    let subtitles = this.data.subtitles
    let time = e.detail.currentTime
    for (var i in subtitles) {
      if (time > subtitles[i].startTime && time < subtitles[i].endTime) {
        let top = subtitles[i].sn > 3 ? subtitles[i].sn > subtitles.length - 3 ? (subtitles.length - 5) * 50 : (subtitles[i].sn - 3) * 50 : 0
        if (top !== this.data.toView) {
          this.setData({
            toView: top,
            selectedIndex: subtitles[i].sn
          })
        }else{
          this.setData({
            selectedIndex: subtitles[i].sn
          })
        }
        
        console.log(this.data.toView, subtitles[i].sn)
      }
    }
  },
  getSrtList: function() {
    let that = this
    wx.request({
      url: 'https://naturling-media.oss-cn-hangzhou.aliyuncs.com/course/subtitle/5d41d82f52247ce73d404757.srt',
      method: "GET",
      success: function(res) {
        console.log(res.data)
        that.parseSrtSubtitles(res.data)
      }
    })
  },
  parseSrtSubtitles: function(srt) {
    var subtitles = [];
    var textSubtitles = srt.split(/\n\s*\n/g); //把空白行split出来
    textSubtitles.splice(12, 3)
    // console.log(textSubtitles)
    for (var i = 0; i < textSubtitles.length; ++i) {
      if (textSubtitles[i] !== '') {
        var textSubtitle = textSubtitles[i].split('\n');
        if (textSubtitle.length >= 2) {
          var sn = textSubtitle[0]; // 字幕的序号
          var startTime = this.toSeconds((textSubtitle[1].split(' --> ')[0]).replace(/(^\s*)|(\s*$)/g, "")); // 字幕的开始时间
          var endTime = this.toSeconds((textSubtitle[1].split(' --> ')[1]).replace(/(^\s*)|(\s*$)/g, "")); // 字幕的结束时间
          var content = textSubtitle[2]; // 字幕的内容

          // 字幕可能有多行
          if (textSubtitle.length > 2) {
            for (var j = 3; j < textSubtitle.length; j++) {
              content += '\n' + textSubtitle[j];
            }
          }
          // let arr=content.split("\n")
          // console.log(arr)
          // 字幕对象
          var subtitle = {
            sn: sn,
            startTime: startTime,
            endTime: endTime,
            content: content
          };
          subtitles.push(subtitle);
        }
      }
    }
    console.log(subtitles)
    this.setData({
      subtitles: subtitles
    })
  },
  toSeconds: function(t) {
    var s = 0.0;

    if (t) {
      var p = t.split(':');
      console.log(p)
      for (var i = 0; i < p.length; i++) {
        s = s * 60 + parseFloat(p[i].replace(',', '.'));
      }
    }

    return s;
  },
  clickInto: function() {
    // let arr = this.data.subtitles
    // arr.map(item => {
    //   var query = wx.createSelectorQuery();
    //   query.select(`#scroll_${item.sn}`).boundingClientRect()
    //   query.exec((res) => {
    //     // var containerHeight = res[0].height;
    //     // var listHeight = res[1].height;
    //     console.log(res)
    //     if (res[0].top - 300 < 0) {
    //       item.top = 0
    //     } else {
    //       item.top = res[0].top - 300
    //     }
    //   })
    // })
    // console.log(arr)
    // console.log(this.data.subtitles)
    this.setData({
      toView: 550
    })
  }
})