// pages/myTest/myTest.js
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    this.drawMyCanvas()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  drawMyCanvas: function() {
    let windowWidth=0
    const PI=Math.PI
    wx.getSystemInfo({
      success: function (res) {
        windowWidth=res.windowWidth
        // 高度,宽度 单位为px
      }
    })

    let arcX = windowWidth / 2 /**圆心的X**/
    let arcY = 108 //圆心的Y
    let radius = 75 //半径
    let maxNum=100  //最高值
    let ratio=(radius/maxNum)  /**半径和最高值的比值**/  
    // let l1 = 20
    // let l2 = 60
    // let l3 = 100
    // let l4 = 70
    let fontArr = [{
      name: "听力",
      level: "Level2",
      levelNum: 30
    }, {
      name: "语法",
      level: "Level2",
      levelNum: 40
    }, {
      name: "自然拼读",
      level: "Level2",
      levelNum: 50
    }, {
      name: "阅读",
      level: "Level2",
      levelNum: 80
    }]
    let ctx = wx.createCanvasContext("myCanvas")

    ctx.beginPath()
    ctx.moveTo(arcX - radius, arcY)
    ctx.lineTo(arcX + radius, arcY)
    ctx.moveTo(arcX, arcY - radius)
    ctx.lineTo(arcX, arcY + radius)
    ctx.setStrokeStyle('#AAAAAA')
    ctx.stroke()
    //画圆(小圆)
    ctx.beginPath()
    ctx.arc(arcX, arcY, radius / 3, 0, 2 * Math.PI)
    ctx.setStrokeStyle('#AAAAAA')
    ctx.stroke()

    //中圆
    ctx.beginPath()
    ctx.arc(arcX, arcY, radius / 3 * 2, 0, 2 * Math.PI)
    ctx.setStrokeStyle('#AAAAAA')
    ctx.stroke()
    //大圆
    ctx.beginPath()
    ctx.arc(arcX, arcY, radius, 0, 2 * Math.PI)
    ctx.setStrokeStyle('#AAAAAA')
    ctx.stroke()
    // ctx.setFillStyle('#fff')
    // ctx.fill()
    //不规则菱形
    ctx.beginPath()
    ctx.moveTo(arcX, arcY)
    for (var i in fontArr){
      if(i==0){
        ctx.lineTo(arcX, arcY - fontArr[i].levelNum * ratio)
      }else if (i==1){
        ctx.lineTo(arcX + fontArr[i].levelNum * ratio, arcY)
      }else if (i==2){
        ctx.lineTo(arcX, arcY + fontArr[i].levelNum * ratio)
      }else if (i==3){
        ctx.lineTo(arcX - fontArr[i].levelNum * ratio, arcY)
      }
    }
    console.log(parseInt((Math.cos(Math.PI/2))))
    ctx.lineTo(arcX, arcY - fontArr[0].levelNum * ratio)
    
    ctx.setFillStyle('rgba(98,210,180,0.6)')
    ctx.fill()

    //定点样式
    ctx.beginPath()
    ctx.setFillStyle('rgba(98,210,180,1)')
    ctx.setStrokeStyle('#fff')
    for (var i in fontArr) {
      if (i == 0) {
        ctx.rect(arcX - 3, arcY - fontArr[i].levelNum * ratio - 3, 6, 6)
      } else if (i == 1) {
        ctx.rect(arcX + fontArr[i].levelNum * ratio - 3, arcY - 3, 6, 6)
      } else if (i == 2) {
        ctx.rect(arcX - 3, arcY + fontArr[i].levelNum * ratio - 3, 6, 6)
      } else if (i == 3) {
        ctx.rect(arcX - fontArr[i].levelNum * ratio - 3, arcY - 3, 6, 6)
      }
    }
    ctx.fill()
    ctx.stroke()
    //字体
    ctx.beginPath()
    
    ctx.setFillStyle('#333')
    ctx.setFontSize(12)
    ctx.setTextAlign('center')

    for (var i in fontArr) {
      if(i==0){
        ctx.fillText(fontArr[i].name, arcX, arcY - radius - 22)
        ctx.fillText(fontArr[i].level, arcX, arcY - radius - 7)
      }else if (i==1){
        ctx.fillText(fontArr[i].name, arcX + radius + 30, arcY - 3)
        ctx.fillText(fontArr[i].level, arcX + radius + 30, arcY + 12)
      }else if (i==2){
        ctx.fillText(fontArr[i].name, arcX, arcY + radius + 18)
        ctx.fillText(fontArr[i].level, arcX, arcY + radius + 33)
      }else if (i==3){
        ctx.fillText(fontArr[i].name, arcX - radius - 25, arcY - 3)
        ctx.fillText(fontArr[i].level, arcX - radius - 25, arcY + 12)
      }
    }
    ctx.draw()

  }
})