// pages/bookPageAnima/bookPageAnima.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    books: [{
        animation: false,
        displayType: "block"
      },
      {
        animation: false,
        displayType: "none"
      },
      {
        animation: false,
        displayType: "none"
      }
    ],
    pageIndex: -1,
    clickStatus: true //是否可点击下一页
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  clickNextPage: function() {
    if (this.data.clickStatus) {
      //点击下一页
      let index = this.data.pageIndex + 1
      // let typeIndex=this.data.pageIndex+2
      // console.log(index+1)
      if(!(index+1>this.data.books.length-1)){
        this.setData({
          [`books[${index + 1}].displayType`]: "block",
        })
      }
      if (index > this.data.books.length-1) {
        //如果没有下一页就return
        return
      } else {
        this.setData({
          pageIndex: index,
          [`books[${index}].animation`]: true,
          clickStatus: false
        })
      }

      //动画过渡2s
      setTimeout(() => {
        this.setData({
          clickStatus: true
        })
      }, 2000)
    }

  }
})