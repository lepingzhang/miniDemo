// pages/srt/srt.js
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    this.getSrtList()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  getSrtList: function() {
    let that = this
    wx.request({
      url: 'https://naturling-media.oss-cn-hangzhou.aliyuncs.com/temp/course_1675_subtitle.srt',
      method: "GET",
      success: function(res) {
        console.log(res.data)
        that.parseSrtSubtitles(res.data)
      }
    })
  },
  parseSrtSubtitles: function(srt) {
    var subtitles = [];
    var textSubtitles = srt.split(/\n\s*\n/g); //把空白行split出来
    console.log(textSubtitles)
    for (var i = 0; i < textSubtitles.length; ++i) {
      if (textSubtitles[i] !== '') {
        var textSubtitle = textSubtitles[i].split('\n');
        if (textSubtitle.length >= 2) {
          var sn = textSubtitle[0]; // 字幕的序号
          var startTime = this.toSeconds((textSubtitle[1].split(' --> ')[0]).replace(/(^\s*)|(\s*$)/g, "")); // 字幕的开始时间
          var endTime = this.toSeconds((textSubtitle[1].split(' --> ')[1]).replace(/(^\s*)|(\s*$)/g, "")); // 字幕的结束时间
          var content = textSubtitle[2]; // 字幕的内容

          // 字幕可能有多行
          if (textSubtitle.length > 2) {
            for (var j = 3; j < textSubtitle.length; j++) {
              content += '\n' + textSubtitle[j];
            }
          }

          // 字幕对象
          var subtitle = {
            sn: sn,
            startTime: startTime,
            endTime: endTime,
            content: content
          };

          subtitles.push(subtitle);

        }
      }
    }
    console.log(subtitles)
  },
  toSeconds: function(t) {
    var s = 0.0;

    if (t) {
      var p = t.split(':');
      for (var i = 0; i < p.length; i++) {
        s = s * 60 + parseFloat(p[i].replace(',', '.'));
      }
    }
    return s;
  }
})