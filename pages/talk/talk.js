// pages/talk/talk.js
let innerAudioContext = "" //当前实例
const recorderManager = wx.getRecorderManager()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    talkList: [{
        audio: "https://dev.naturling.com/evaluation/file/evaluation/audio/5c43338566a4c0a62446c16c/7.mp3",
        type: "smart",
        playStatus: false,
        animationData: ""
      },
      {
        audio: "https://dev.naturling.com/evaluation/file/evaluation/audio/5c43338966a4c0a62446c5cc/9.mp3",
        type: "smart",
        playStatus: false,
        animationData: ""
      },
      {
        audio: "https://dev.naturling.com/evaluation/file/evaluation/audio/5c43338566a4c0a62446c16c/7.mp3",
        type: "smart",
        playStatus: false,
        animationData: ""
      }
    ],
    talkAddArr: [],
    num: 0,
    animationData: ""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    const animation = wx.createAnimation({
      duration: 600,
      timingFunction: 'ease',
    })
    this.animation = animation
    this.autoPlay(0)
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {
    innerAudioContext.stop()
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  //事件函数
  startRecord: function() {
    //开始录音
    recorderManager.onStart(() => {
      console.log('recorder start')
    })
    recorderManager.onPause(() => {
      console.log('recorder pause')
    })
    recorderManager.onStop((res) => {
      console.log('recorder stop', res)
      
    })
    recorderManager.onFrameRecorded((res) => {
      const {
        frameBuffer
      } = res
      console.log('frameBuffer.byteLength', frameBuffer.byteLength)
    })
    recorderManager.start()
    console.log(888)
  },
  endRecord: function() {
    //结束录音
    console.log(777)
    recorderManager.stop()
  },
  autoPlay: function(num) {
    var obj = this.data.talkList[num]
    let that = this
    if (obj) {
      that.setData({
        [`talkAddArr[${num}]`]: obj
      })
      //动画
      setTimeout(() => {
        this.animation.opacity(1).translateY(10).step()
        that.setData({
          [`talkAddArr[${num}].animationData`]: this.animation.export()
        })
      }, 100)

      //播放音频
      innerAudioContext = wx.createInnerAudioContext()
      innerAudioContext.autoplay = true
      innerAudioContext.src = obj.audio
      innerAudioContext.onPlay(() => {
        console.log('开始播放')
        that.setData({
          [`talkAddArr[${num}].playStatus`]: true
        })
      })
      innerAudioContext.onEnded((res) => {
        console.log("播放结束")
        that.setData({
          [`talkAddArr[${num}].playStatus`]: false,
          num: parseInt(that.data.num) + 1
        })
        that.autoPlay(that.data.num)
      })
    } else {
      return
    }

  }
})