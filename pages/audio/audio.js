// pages/audio/audio.js
const innerAudioContext = wx.createInnerAudioContext()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    duration: "",
    currentTime: "",
    progressWidth: 0
  },
  
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.audioLoad()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {
    innerAudioContext.stop()
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  //音频加载
  audioLoad: function() {
    let that = this
    let width = 100
    innerAudioContext.src = 'http://ws.stream.qqmusic.qq.com/M500001VfvsJ21xFqb.mp3?guid=ffffffff82def4af4b12b3cd9337d5e7&uin=346897220&vkey=6292F51E1E384E061FF02C31F716658E5C81F5594D561F2E88B854E81CAAB7806D5E4F103E55D33C16F3FAC506D1AB172DE8600B37E43FAD&fromtag=46';
    innerAudioContext.autoplay=true
    innerAudioContext.onPlay(() => {
      console.log('开始播放')
    })
    innerAudioContext.onError((res) => {
      console.log(res.errMsg)
      console.log(res.errCode)
    })
    setTimeout(()=>{
      console.log(innerAudioContext.duration)
    },1000)
  },
  //事件函数
  playClick: function() {
    let that = this
    let width = 100
    innerAudioContext.play()

    this.setData({
      duration: innerAudioContext.duration
    })
    innerAudioContext.onTimeUpdate((res) => {
      that.setData({
        progressWidth: innerAudioContext.currentTime * (width / innerAudioContext.duration)
      })
      console.log(innerAudioContext.currentTime)
    })

  },
  pauseClick: function() {
    //暂停
    innerAudioContext.pause()
  },
  audioChange: function(e) {
    let that=this
    let width=100
    let index = e.detail.value
    let second = innerAudioContext.duration * (e.detail.value / width)
    innerAudioContext.seek(second)
    innerAudioContext.onTimeUpdate((res) => {
      that.setData({
        progressWidth: innerAudioContext.currentTime * (width / innerAudioContext.duration)
      })
      console.log(innerAudioContext.currentTime)
    })
  },
  audioChanging:function(){
    innerAudioContext.offTimeUpdate()
  }
})