// pages/myTestLine/myTestLine.js
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.createLineCanvas()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  createLineCanvas: function() {
    let ctx = wx.createCanvasContext("canvasLine")
    let windowWidth = 0
    let data = [{
        time: 1,
        level: 1
      },
      {
        time: 2,
        level: 2
      },
      {
        time: 3,
        level: 1
      },
      {
        time: 4,
        level: 1
      },
      {
        time: 5,
        level: 4
      },
      {
        time: 6,
        level: 1
      },
      {
        time: 7,
        level: 1
      },
      {
        time: 80,
        level: 5
      }
    ]

    let dateDistance = data[data.length - 1].time - data[0].time //时间差
    wx.getSystemInfo({
      success: function(res) {
        windowWidth = res.windowWidth
        // 高度,宽度 单位为px
      }
    })
    let canvasWidth = windowWidth / 1.3
    let canvasHeight = 180
    let levelMax = 9 //最高等级
    let coordinateLine = 8 //坐标线长
    let top = 23 //图表距离顶部的距离
    //图表距离屏幕左边的距离
    let x = (windowWidth - canvasWidth) / 2
    let yArr = [9, 8, 7, 6, 5, 4, 3, 2, 1] //y轴的数据
    let yFontSize = 14 //y轴的数据字体
    //y轴的间距
    let coordinateLineDistance = canvasHeight / levelMax
    //纵坐标
    ctx.beginPath()
    ctx.lineWidth = 0.6
    ctx.moveTo(x, top)
    ctx.lineTo(x, canvasHeight + top)
    ctx.setFillStyle('#333333')
    ctx.setFontSize(yFontSize)
    ctx.setTextAlign('center')
    ctx.fillText('Level', x - 10, top - 10)

    for (var i = 0; i < levelMax; i++) {
      ctx.lineWidth = 1
      ctx.moveTo(x, canvasHeight / levelMax * i + top)
      ctx.lineTo(x - coordinateLine, canvasHeight / levelMax * i + top)
      ctx.lineWidth = 0.6
      ctx.moveTo(x, canvasHeight / levelMax * i + top)
      ctx.lineTo(x + canvasWidth, canvasHeight / levelMax * i + top)
      ctx.fillText(yArr[i], x - coordinateLine - 6, top + yFontSize / 2 + canvasHeight / levelMax * i)
      ctx.setStrokeStyle('#999999')
    }
    //横坐标
    ctx.moveTo(x, canvasHeight + top)
    ctx.lineTo(canvasWidth + x, canvasHeight + top)
    ctx.setStrokeStyle('#999999')


    if (dateDistance <= 30) {
      //半年之内所测时间不超过一个月
      let weekArr = ['week1', 'week2', 'week3', 'week4']
      for (var i = 1; i <= 4; i++) {
        ctx.moveTo(x + canvasWidth / 4 * i, canvasHeight + top)
        ctx.lineTo(x + canvasWidth / 4 * i, canvasHeight + top + coordinateLine)
        ctx.fillText(weekArr[i - 1], x + canvasWidth / 4 * i - canvasWidth / 8, canvasHeight + top + 15)
      }
      ctx.stroke()
      //数据点
      //y轴的间距

      for (var i in data) {
        //距离第一个的时间差值
        let distanceFirst = data[i].time - data[0].time
        ctx.beginPath()
        ctx.arc(x + canvasWidth / 30 * distanceFirst, canvasHeight + top - coordinateLineDistance * data[i].level, 3, 0, 2 * Math.PI)
        ctx.setFillStyle('#62D2B4')
        ctx.fill()
        ctx.beginPath()
        ctx.arc(x + canvasWidth / 30 * distanceFirst, canvasHeight + top - coordinateLineDistance * data[i].level, 3, 0, 2 * Math.PI)
        ctx.setStrokeStyle('#fff')
        ctx.stroke()
      }
    } else {
      let monthArr = []
      if (dateDistance <= 60) {
        monthArr = ['2019/1', '2019/2']
      } else if (dateDistance <= 90) {
        monthArr = ['2019/1', '2019/2', '2019/3']
      } else if (dateDistance <= 120) {
        monthArr = ['2019/1', '2019/2', '2019/3', '2019/4']
      } else if (dateDistance <= 150) {
        monthArr = ['2019/1', '2019/2', '2019/3', '2019/4', '2019/5']
      } else if (dateDistance <= 180) {
        monthArr = ['2019/1', '2019/2', '2019/3', '2019/4', '2019/5', '2019/6']
      }
      ctx.stroke()
      for (var i = 1; i <= monthArr.length; i++) {
        ctx.beginPath()
        ctx.moveTo(x + canvasWidth / monthArr.length * i, canvasHeight + top)
        ctx.lineTo(x + canvasWidth / monthArr.length * i, canvasHeight + top + coordinateLine)
        ctx.stroke()
        ctx.save();
        ctx.translate(x + canvasWidth / monthArr.length * i - canvasWidth / monthArr.length / 2, canvasHeight + top + 15);
        ctx.rotate(-30 * Math.PI / 180)
        ctx.fillText(monthArr[i - 1], -10, 3)
        ctx.restore()
        ctx.stroke()
      }

      for (var i in data) {
        //距离第一个的时间差值
        let distanceFirst = data[i].time - data[0].time
        ctx.beginPath()
        ctx.arc(x + canvasWidth / (30 * monthArr.length) * distanceFirst, canvasHeight + top - coordinateLineDistance * data[i].level, 3, 0, 2 * Math.PI)
        ctx.setFillStyle('#62D2B4')
        ctx.fill()
        ctx.beginPath()
        ctx.arc(x + canvasWidth / (30 * monthArr.length) * distanceFirst, canvasHeight + top - coordinateLineDistance * data[i].level, 3, 0, 2 * Math.PI)
        ctx.setStrokeStyle('#fff')
        ctx.stroke()
      }
    }
    ctx.draw()
  }
})